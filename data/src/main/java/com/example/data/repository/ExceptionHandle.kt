package com.example.data.repository

import com.example.domain.models.ErrorType
import com.example.domain.models.Result
import java.net.UnknownHostException

interface ExceptionHandle {

    fun handle(e: Exception): Result

    class Base: ExceptionHandle {
        override fun handle(e: Exception): Result = Result.Fail(
            when (e) {
                is UnknownHostException -> ErrorType.NO_CONNECTION
                is NullPointerException -> ErrorType.NULL_POINTER_EXCEPTION
                else -> ErrorType.GENERIC_ERROR
            }
        )
    }
}
