package com.example.data.repository

import com.example.domain.models.Result
import com.example.domain.repository.Repository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryImpl @Inject constructor(
    private val cloudSource: CloudSource,
    private val exceptionHandle: ExceptionHandle
) : Repository {

    override val allItems: Result
        get() = try {
            Result.Success(cloudSource.fetchCloud())
        } catch (e: Exception) {
            exceptionHandle.handle(e)
        }
}





