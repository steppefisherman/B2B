package com.example.b2b.screens.explorerFragment.phones

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.b2b.R
import com.example.b2b.adapters.phonesFragmentAdapter.PhonesFragmentAdapter
import com.example.b2b.databinding.FragmentPhonesBinding
import com.example.b2b.screens.BaseFragment
import com.example.b2b.utils.showSnackIndefinite
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhonesFragment : BaseFragment<FragmentPhonesBinding>() {

    private val vm by viewModels<PhonesFragmentViewModel>()
    private var mAdapter: PhonesFragmentAdapter = PhonesFragmentAdapter()

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentPhonesBinding.inflate(inflater, container, false)

    override fun onStart() {
        super.onStart()
        sendDataToAdapter()
    }

    private fun sendDataToAdapter() {
        mBinding.phonesFragmentRv.adapter = mAdapter

        vm.allItems.observe(viewLifecycleOwner) { listDataApp ->
            listDataApp?.let { mAdapter.setData(it) }
        }

        vm.error.observe(viewLifecycleOwner) {
            when (it.ordinal) {
                0 -> view?.showSnackIndefinite(R.string.no_connection_message)
                1 -> view?.showSnackIndefinite(R.string.null_pointer_exception)
                2 -> view?.showSnackIndefinite(R.string.something_went_wrong)
            }
        }
    }
}